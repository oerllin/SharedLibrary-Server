package pers.hl.library.po;

import pers.hl.library.common.base.IExample;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BookShareRecExample implements IExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public BookShareRecExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andBsrIdIsNull() {
            addCriterion("bsr_id is null");
            return (Criteria) this;
        }

        public Criteria andBsrIdIsNotNull() {
            addCriterion("bsr_id is not null");
            return (Criteria) this;
        }

        public Criteria andBsrIdEqualTo(Integer value) {
            addCriterion("bsr_id =", value, "bsrId");
            return (Criteria) this;
        }

        public Criteria andBsrIdNotEqualTo(Integer value) {
            addCriterion("bsr_id <>", value, "bsrId");
            return (Criteria) this;
        }

        public Criteria andBsrIdGreaterThan(Integer value) {
            addCriterion("bsr_id >", value, "bsrId");
            return (Criteria) this;
        }

        public Criteria andBsrIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("bsr_id >=", value, "bsrId");
            return (Criteria) this;
        }

        public Criteria andBsrIdLessThan(Integer value) {
            addCriterion("bsr_id <", value, "bsrId");
            return (Criteria) this;
        }

        public Criteria andBsrIdLessThanOrEqualTo(Integer value) {
            addCriterion("bsr_id <=", value, "bsrId");
            return (Criteria) this;
        }

        public Criteria andBsrIdIn(List<Integer> values) {
            addCriterion("bsr_id in", values, "bsrId");
            return (Criteria) this;
        }

        public Criteria andBsrIdNotIn(List<Integer> values) {
            addCriterion("bsr_id not in", values, "bsrId");
            return (Criteria) this;
        }

        public Criteria andBsrIdBetween(Integer value1, Integer value2) {
            addCriterion("bsr_id between", value1, value2, "bsrId");
            return (Criteria) this;
        }

        public Criteria andBsrIdNotBetween(Integer value1, Integer value2) {
            addCriterion("bsr_id not between", value1, value2, "bsrId");
            return (Criteria) this;
        }

        public Criteria andBsrUserIdIsNull() {
            addCriterion("bsr_user_id is null");
            return (Criteria) this;
        }

        public Criteria andBsrUserIdIsNotNull() {
            addCriterion("bsr_user_id is not null");
            return (Criteria) this;
        }

        public Criteria andBsrUserIdEqualTo(Integer value) {
            addCriterion("bsr_user_id =", value, "bsrUserId");
            return (Criteria) this;
        }

        public Criteria andBsrUserIdNotEqualTo(Integer value) {
            addCriterion("bsr_user_id <>", value, "bsrUserId");
            return (Criteria) this;
        }

        public Criteria andBsrUserIdGreaterThan(Integer value) {
            addCriterion("bsr_user_id >", value, "bsrUserId");
            return (Criteria) this;
        }

        public Criteria andBsrUserIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("bsr_user_id >=", value, "bsrUserId");
            return (Criteria) this;
        }

        public Criteria andBsrUserIdLessThan(Integer value) {
            addCriterion("bsr_user_id <", value, "bsrUserId");
            return (Criteria) this;
        }

        public Criteria andBsrUserIdLessThanOrEqualTo(Integer value) {
            addCriterion("bsr_user_id <=", value, "bsrUserId");
            return (Criteria) this;
        }

        public Criteria andBsrUserIdIn(List<Integer> values) {
            addCriterion("bsr_user_id in", values, "bsrUserId");
            return (Criteria) this;
        }

        public Criteria andBsrUserIdNotIn(List<Integer> values) {
            addCriterion("bsr_user_id not in", values, "bsrUserId");
            return (Criteria) this;
        }

        public Criteria andBsrUserIdBetween(Integer value1, Integer value2) {
            addCriterion("bsr_user_id between", value1, value2, "bsrUserId");
            return (Criteria) this;
        }

        public Criteria andBsrUserIdNotBetween(Integer value1, Integer value2) {
            addCriterion("bsr_user_id not between", value1, value2, "bsrUserId");
            return (Criteria) this;
        }

        public Criteria andBsrRemarkIsNull() {
            addCriterion("bsr_remark is null");
            return (Criteria) this;
        }

        public Criteria andBsrRemarkIsNotNull() {
            addCriterion("bsr_remark is not null");
            return (Criteria) this;
        }

        public Criteria andBsrRemarkEqualTo(String value) {
            addCriterion("bsr_remark =", value, "bsrRemark");
            return (Criteria) this;
        }

        public Criteria andBsrRemarkNotEqualTo(String value) {
            addCriterion("bsr_remark <>", value, "bsrRemark");
            return (Criteria) this;
        }

        public Criteria andBsrRemarkGreaterThan(String value) {
            addCriterion("bsr_remark >", value, "bsrRemark");
            return (Criteria) this;
        }

        public Criteria andBsrRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("bsr_remark >=", value, "bsrRemark");
            return (Criteria) this;
        }

        public Criteria andBsrRemarkLessThan(String value) {
            addCriterion("bsr_remark <", value, "bsrRemark");
            return (Criteria) this;
        }

        public Criteria andBsrRemarkLessThanOrEqualTo(String value) {
            addCriterion("bsr_remark <=", value, "bsrRemark");
            return (Criteria) this;
        }

        public Criteria andBsrRemarkLike(String value) {
            addCriterion("bsr_remark like", value, "bsrRemark");
            return (Criteria) this;
        }

        public Criteria andBsrRemarkNotLike(String value) {
            addCriterion("bsr_remark not like", value, "bsrRemark");
            return (Criteria) this;
        }

        public Criteria andBsrRemarkIn(List<String> values) {
            addCriterion("bsr_remark in", values, "bsrRemark");
            return (Criteria) this;
        }

        public Criteria andBsrRemarkNotIn(List<String> values) {
            addCriterion("bsr_remark not in", values, "bsrRemark");
            return (Criteria) this;
        }

        public Criteria andBsrRemarkBetween(String value1, String value2) {
            addCriterion("bsr_remark between", value1, value2, "bsrRemark");
            return (Criteria) this;
        }

        public Criteria andBsrRemarkNotBetween(String value1, String value2) {
            addCriterion("bsr_remark not between", value1, value2, "bsrRemark");
            return (Criteria) this;
        }

        public Criteria andBsrCreateTimeIsNull() {
            addCriterion("bsr_create_time is null");
            return (Criteria) this;
        }

        public Criteria andBsrCreateTimeIsNotNull() {
            addCriterion("bsr_create_time is not null");
            return (Criteria) this;
        }

        public Criteria andBsrCreateTimeEqualTo(Date value) {
            addCriterion("bsr_create_time =", value, "bsrCreateTime");
            return (Criteria) this;
        }

        public Criteria andBsrCreateTimeNotEqualTo(Date value) {
            addCriterion("bsr_create_time <>", value, "bsrCreateTime");
            return (Criteria) this;
        }

        public Criteria andBsrCreateTimeGreaterThan(Date value) {
            addCriterion("bsr_create_time >", value, "bsrCreateTime");
            return (Criteria) this;
        }

        public Criteria andBsrCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("bsr_create_time >=", value, "bsrCreateTime");
            return (Criteria) this;
        }

        public Criteria andBsrCreateTimeLessThan(Date value) {
            addCriterion("bsr_create_time <", value, "bsrCreateTime");
            return (Criteria) this;
        }

        public Criteria andBsrCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("bsr_create_time <=", value, "bsrCreateTime");
            return (Criteria) this;
        }

        public Criteria andBsrCreateTimeIn(List<Date> values) {
            addCriterion("bsr_create_time in", values, "bsrCreateTime");
            return (Criteria) this;
        }

        public Criteria andBsrCreateTimeNotIn(List<Date> values) {
            addCriterion("bsr_create_time not in", values, "bsrCreateTime");
            return (Criteria) this;
        }

        public Criteria andBsrCreateTimeBetween(Date value1, Date value2) {
            addCriterion("bsr_create_time between", value1, value2, "bsrCreateTime");
            return (Criteria) this;
        }

        public Criteria andBsrCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("bsr_create_time not between", value1, value2, "bsrCreateTime");
            return (Criteria) this;
        }

        public Criteria andBookIdIsNull() {
            addCriterion("book_id is null");
            return (Criteria) this;
        }

        public Criteria andBookIdIsNotNull() {
            addCriterion("book_id is not null");
            return (Criteria) this;
        }

        public Criteria andBookIdEqualTo(Integer value) {
            addCriterion("book_id =", value, "bookId");
            return (Criteria) this;
        }

        public Criteria andBookIdNotEqualTo(Integer value) {
            addCriterion("book_id <>", value, "bookId");
            return (Criteria) this;
        }

        public Criteria andBookIdGreaterThan(Integer value) {
            addCriterion("book_id >", value, "bookId");
            return (Criteria) this;
        }

        public Criteria andBookIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("book_id >=", value, "bookId");
            return (Criteria) this;
        }

        public Criteria andBookIdLessThan(Integer value) {
            addCriterion("book_id <", value, "bookId");
            return (Criteria) this;
        }

        public Criteria andBookIdLessThanOrEqualTo(Integer value) {
            addCriterion("book_id <=", value, "bookId");
            return (Criteria) this;
        }

        public Criteria andBookIdIn(List<Integer> values) {
            addCriterion("book_id in", values, "bookId");
            return (Criteria) this;
        }

        public Criteria andBookIdNotIn(List<Integer> values) {
            addCriterion("book_id not in", values, "bookId");
            return (Criteria) this;
        }

        public Criteria andBookIdBetween(Integer value1, Integer value2) {
            addCriterion("book_id between", value1, value2, "bookId");
            return (Criteria) this;
        }

        public Criteria andBookIdNotBetween(Integer value1, Integer value2) {
            addCriterion("book_id not between", value1, value2, "bookId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}