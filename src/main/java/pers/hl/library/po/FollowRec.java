package pers.hl.library.po;

import lombok.Data;

import java.util.Date;

@Data
public class FollowRec {
    private Integer frId;

    private Integer frUserId;

    private Integer frFollowerId;

    private Date frCreateTime;

    private Date frUpdateTime;

    private User user;

    private User follower;

}