package pers.hl.library.po;

import lombok.Data;

import java.util.Date;

@Data
public class BookShareRec {
    private Integer bsrId;

    private Integer bsrUserId;

    private String bsrRemark;

    private Date bsrCreateTime;

    private Integer bookId;

    private Book book;

    private BookBorRec borrowRecord;

}