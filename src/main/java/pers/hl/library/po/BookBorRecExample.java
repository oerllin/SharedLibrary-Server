package pers.hl.library.po;

import pers.hl.library.common.base.IExample;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BookBorRecExample implements IExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public BookBorRecExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andBbrIdIsNull() {
            addCriterion("bbr_id is null");
            return (Criteria) this;
        }

        public Criteria andBbrIdIsNotNull() {
            addCriterion("bbr_id is not null");
            return (Criteria) this;
        }

        public Criteria andBbrIdEqualTo(Integer value) {
            addCriterion("bbr_id =", value, "bbrId");
            return (Criteria) this;
        }

        public Criteria andBbrIdNotEqualTo(Integer value) {
            addCriterion("bbr_id <>", value, "bbrId");
            return (Criteria) this;
        }

        public Criteria andBbrIdGreaterThan(Integer value) {
            addCriterion("bbr_id >", value, "bbrId");
            return (Criteria) this;
        }

        public Criteria andBbrIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("bbr_id >=", value, "bbrId");
            return (Criteria) this;
        }

        public Criteria andBbrIdLessThan(Integer value) {
            addCriterion("bbr_id <", value, "bbrId");
            return (Criteria) this;
        }

        public Criteria andBbrIdLessThanOrEqualTo(Integer value) {
            addCriterion("bbr_id <=", value, "bbrId");
            return (Criteria) this;
        }

        public Criteria andBbrIdIn(List<Integer> values) {
            addCriterion("bbr_id in", values, "bbrId");
            return (Criteria) this;
        }

        public Criteria andBbrIdNotIn(List<Integer> values) {
            addCriterion("bbr_id not in", values, "bbrId");
            return (Criteria) this;
        }

        public Criteria andBbrIdBetween(Integer value1, Integer value2) {
            addCriterion("bbr_id between", value1, value2, "bbrId");
            return (Criteria) this;
        }

        public Criteria andBbrIdNotBetween(Integer value1, Integer value2) {
            addCriterion("bbr_id not between", value1, value2, "bbrId");
            return (Criteria) this;
        }

        public Criteria andBbrBorrowerIsNull() {
            addCriterion("bbr_borrower is null");
            return (Criteria) this;
        }

        public Criteria andBbrBorrowerIsNotNull() {
            addCriterion("bbr_borrower is not null");
            return (Criteria) this;
        }

        public Criteria andBbrBorrowerEqualTo(Integer value) {
            addCriterion("bbr_borrower =", value, "bbrBorrower");
            return (Criteria) this;
        }

        public Criteria andBbrBorrowerNotEqualTo(Integer value) {
            addCriterion("bbr_borrower <>", value, "bbrBorrower");
            return (Criteria) this;
        }

        public Criteria andBbrBorrowerGreaterThan(Integer value) {
            addCriterion("bbr_borrower >", value, "bbrBorrower");
            return (Criteria) this;
        }

        public Criteria andBbrBorrowerGreaterThanOrEqualTo(Integer value) {
            addCriterion("bbr_borrower >=", value, "bbrBorrower");
            return (Criteria) this;
        }

        public Criteria andBbrBorrowerLessThan(Integer value) {
            addCriterion("bbr_borrower <", value, "bbrBorrower");
            return (Criteria) this;
        }

        public Criteria andBbrBorrowerLessThanOrEqualTo(Integer value) {
            addCriterion("bbr_borrower <=", value, "bbrBorrower");
            return (Criteria) this;
        }

        public Criteria andBbrBorrowerIn(List<Integer> values) {
            addCriterion("bbr_borrower in", values, "bbrBorrower");
            return (Criteria) this;
        }

        public Criteria andBbrBorrowerNotIn(List<Integer> values) {
            addCriterion("bbr_borrower not in", values, "bbrBorrower");
            return (Criteria) this;
        }

        public Criteria andBbrBorrowerBetween(Integer value1, Integer value2) {
            addCriterion("bbr_borrower between", value1, value2, "bbrBorrower");
            return (Criteria) this;
        }

        public Criteria andBbrBorrowerNotBetween(Integer value1, Integer value2) {
            addCriterion("bbr_borrower not between", value1, value2, "bbrBorrower");
            return (Criteria) this;
        }

        public Criteria andBbrTimeIsNull() {
            addCriterion("bbr_time is null");
            return (Criteria) this;
        }

        public Criteria andBbrTimeIsNotNull() {
            addCriterion("bbr_time is not null");
            return (Criteria) this;
        }

        public Criteria andBbrTimeEqualTo(Date value) {
            addCriterion("bbr_time =", value, "bbrTime");
            return (Criteria) this;
        }

        public Criteria andBbrTimeNotEqualTo(Date value) {
            addCriterion("bbr_time <>", value, "bbrTime");
            return (Criteria) this;
        }

        public Criteria andBbrTimeGreaterThan(Date value) {
            addCriterion("bbr_time >", value, "bbrTime");
            return (Criteria) this;
        }

        public Criteria andBbrTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("bbr_time >=", value, "bbrTime");
            return (Criteria) this;
        }

        public Criteria andBbrTimeLessThan(Date value) {
            addCriterion("bbr_time <", value, "bbrTime");
            return (Criteria) this;
        }

        public Criteria andBbrTimeLessThanOrEqualTo(Date value) {
            addCriterion("bbr_time <=", value, "bbrTime");
            return (Criteria) this;
        }

        public Criteria andBbrTimeIn(List<Date> values) {
            addCriterion("bbr_time in", values, "bbrTime");
            return (Criteria) this;
        }

        public Criteria andBbrTimeNotIn(List<Date> values) {
            addCriterion("bbr_time not in", values, "bbrTime");
            return (Criteria) this;
        }

        public Criteria andBbrTimeBetween(Date value1, Date value2) {
            addCriterion("bbr_time between", value1, value2, "bbrTime");
            return (Criteria) this;
        }

        public Criteria andBbrTimeNotBetween(Date value1, Date value2) {
            addCriterion("bbr_time not between", value1, value2, "bbrTime");
            return (Criteria) this;
        }

        public Criteria andBbrBookIdIsNull() {
            addCriterion("bbr_book_id is null");
            return (Criteria) this;
        }

        public Criteria andBbrBookIdIsNotNull() {
            addCriterion("bbr_book_id is not null");
            return (Criteria) this;
        }

        public Criteria andBbrBookIdEqualTo(Integer value) {
            addCriterion("bbr_book_id =", value, "bbrBookId");
            return (Criteria) this;
        }

        public Criteria andBbrBookIdNotEqualTo(Integer value) {
            addCriterion("bbr_book_id <>", value, "bbrBookId");
            return (Criteria) this;
        }

        public Criteria andBbrBookIdGreaterThan(Integer value) {
            addCriterion("bbr_book_id >", value, "bbrBookId");
            return (Criteria) this;
        }

        public Criteria andBbrBookIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("bbr_book_id >=", value, "bbrBookId");
            return (Criteria) this;
        }

        public Criteria andBbrBookIdLessThan(Integer value) {
            addCriterion("bbr_book_id <", value, "bbrBookId");
            return (Criteria) this;
        }

        public Criteria andBbrBookIdLessThanOrEqualTo(Integer value) {
            addCriterion("bbr_book_id <=", value, "bbrBookId");
            return (Criteria) this;
        }

        public Criteria andBbrBookIdIn(List<Integer> values) {
            addCriterion("bbr_book_id in", values, "bbrBookId");
            return (Criteria) this;
        }

        public Criteria andBbrBookIdNotIn(List<Integer> values) {
            addCriterion("bbr_book_id not in", values, "bbrBookId");
            return (Criteria) this;
        }

        public Criteria andBbrBookIdBetween(Integer value1, Integer value2) {
            addCriterion("bbr_book_id between", value1, value2, "bbrBookId");
            return (Criteria) this;
        }

        public Criteria andBbrBookIdNotBetween(Integer value1, Integer value2) {
            addCriterion("bbr_book_id not between", value1, value2, "bbrBookId");
            return (Criteria) this;
        }

        public Criteria andBbrDurationIsNull() {
            addCriterion("bbr_duration is null");
            return (Criteria) this;
        }

        public Criteria andBbrDurationIsNotNull() {
            addCriterion("bbr_duration is not null");
            return (Criteria) this;
        }

        public Criteria andBbrDurationEqualTo(Integer value) {
            addCriterion("bbr_duration =", value, "bbrDuration");
            return (Criteria) this;
        }

        public Criteria andBbrDurationNotEqualTo(Integer value) {
            addCriterion("bbr_duration <>", value, "bbrDuration");
            return (Criteria) this;
        }

        public Criteria andBbrDurationGreaterThan(Integer value) {
            addCriterion("bbr_duration >", value, "bbrDuration");
            return (Criteria) this;
        }

        public Criteria andBbrDurationGreaterThanOrEqualTo(Integer value) {
            addCriterion("bbr_duration >=", value, "bbrDuration");
            return (Criteria) this;
        }

        public Criteria andBbrDurationLessThan(Integer value) {
            addCriterion("bbr_duration <", value, "bbrDuration");
            return (Criteria) this;
        }

        public Criteria andBbrDurationLessThanOrEqualTo(Integer value) {
            addCriterion("bbr_duration <=", value, "bbrDuration");
            return (Criteria) this;
        }

        public Criteria andBbrDurationIn(List<Integer> values) {
            addCriterion("bbr_duration in", values, "bbrDuration");
            return (Criteria) this;
        }

        public Criteria andBbrDurationNotIn(List<Integer> values) {
            addCriterion("bbr_duration not in", values, "bbrDuration");
            return (Criteria) this;
        }

        public Criteria andBbrDurationBetween(Integer value1, Integer value2) {
            addCriterion("bbr_duration between", value1, value2, "bbrDuration");
            return (Criteria) this;
        }

        public Criteria andBbrDurationNotBetween(Integer value1, Integer value2) {
            addCriterion("bbr_duration not between", value1, value2, "bbrDuration");
            return (Criteria) this;
        }

        public Criteria andBbrStatusIsNull() {
            addCriterion("bbr_status is null");
            return (Criteria) this;
        }

        public Criteria andBbrStatusIsNotNull() {
            addCriterion("bbr_status is not null");
            return (Criteria) this;
        }

        public Criteria andBbrStatusEqualTo(Integer value) {
            addCriterion("bbr_status =", value, "bbrStatus");
            return (Criteria) this;
        }

        public Criteria andBbrStatusNotEqualTo(Integer value) {
            addCriterion("bbr_status <>", value, "bbrStatus");
            return (Criteria) this;
        }

        public Criteria andBbrStatusGreaterThan(Integer value) {
            addCriterion("bbr_status >", value, "bbrStatus");
            return (Criteria) this;
        }

        public Criteria andBbrStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("bbr_status >=", value, "bbrStatus");
            return (Criteria) this;
        }

        public Criteria andBbrStatusLessThan(Integer value) {
            addCriterion("bbr_status <", value, "bbrStatus");
            return (Criteria) this;
        }

        public Criteria andBbrStatusLessThanOrEqualTo(Integer value) {
            addCriterion("bbr_status <=", value, "bbrStatus");
            return (Criteria) this;
        }

        public Criteria andBbrStatusIn(List<Integer> values) {
            addCriterion("bbr_status in", values, "bbrStatus");
            return (Criteria) this;
        }

        public Criteria andBbrStatusNotIn(List<Integer> values) {
            addCriterion("bbr_status not in", values, "bbrStatus");
            return (Criteria) this;
        }

        public Criteria andBbrStatusBetween(Integer value1, Integer value2) {
            addCriterion("bbr_status between", value1, value2, "bbrStatus");
            return (Criteria) this;
        }

        public Criteria andBbrStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("bbr_status not between", value1, value2, "bbrStatus");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}