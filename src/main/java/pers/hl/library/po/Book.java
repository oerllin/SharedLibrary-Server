package pers.hl.library.po;

import lombok.Data;

@Data
public class Book {

    public static final Integer STATUS_ENABLE = 0;
    public static final Integer STATUS_UNABLE = 1;

    private Integer bookId;

    private String bookName;

    private Long bookIsbn;

    private Integer bookOwner;

    private Integer bookFileId;

    /**
     * 书籍状态 0：新发布 1：借阅中 2：已借阅(接受) 3：已归还 4：已取消 5：已拒绝 999：无意义，用于标记只修改特殊信息(二维码id)
     */
    private Integer bookStatus;

    private String bookDesc;

    private String bookAuthor;

    private Integer bookTag;

    private Integer bookLibId;

    private String bookImg;

    private String tagName;

    private String ownerName;

    private String libName;

    private Integer collect = 0;

}