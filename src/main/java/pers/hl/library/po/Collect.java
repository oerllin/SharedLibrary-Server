package pers.hl.library.po;

import lombok.Data;

import java.util.Date;

@Data
public class Collect {
    private Integer collectId;

    private Integer bookId;

    private Integer userId;

    private Date createTime;

    private Book book;

}