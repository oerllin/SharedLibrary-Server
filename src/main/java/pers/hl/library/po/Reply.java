package pers.hl.library.po;

import lombok.Data;

import java.util.Date;

@Data
public class Reply {

    private Integer rplId;

    private Integer rplCmtId;

    private Integer rplUserId;

    private String rplUserName;

    private String rplInfo;

    private Date rplTime;

}