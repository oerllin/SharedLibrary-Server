package pers.hl.library.po;

import lombok.Data;

import java.util.Date;

@Data
public class Comment {
    private Integer cmtId;

    private Integer cmtUserId;

    private String cmtInfo;

    private Integer cmtBookId;

    private Date cmtTime;

    private Integer replyCount;

    private String userName;

}