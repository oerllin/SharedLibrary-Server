package pers.hl.library.po;

import lombok.Data;

@Data
public class Library {
    private Integer libId;

    private String libName;

    private String libLocation;

    private Double libLongitude;

    private Double libLatitude;

    /**
     * 距离：米
     */
    private Integer distance;
}