package pers.hl.library.po;

import pers.hl.library.common.base.IExample;

import java.util.ArrayList;
import java.util.List;

public class LibraryExample implements IExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public LibraryExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andLibIdIsNull() {
            addCriterion("lib_id is null");
            return (Criteria) this;
        }

        public Criteria andLibIdIsNotNull() {
            addCriterion("lib_id is not null");
            return (Criteria) this;
        }

        public Criteria andLibIdEqualTo(Integer value) {
            addCriterion("lib_id =", value, "libId");
            return (Criteria) this;
        }

        public Criteria andLibIdNotEqualTo(Integer value) {
            addCriterion("lib_id <>", value, "libId");
            return (Criteria) this;
        }

        public Criteria andLibIdGreaterThan(Integer value) {
            addCriterion("lib_id >", value, "libId");
            return (Criteria) this;
        }

        public Criteria andLibIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("lib_id >=", value, "libId");
            return (Criteria) this;
        }

        public Criteria andLibIdLessThan(Integer value) {
            addCriterion("lib_id <", value, "libId");
            return (Criteria) this;
        }

        public Criteria andLibIdLessThanOrEqualTo(Integer value) {
            addCriterion("lib_id <=", value, "libId");
            return (Criteria) this;
        }

        public Criteria andLibIdIn(List<Integer> values) {
            addCriterion("lib_id in", values, "libId");
            return (Criteria) this;
        }

        public Criteria andLibIdNotIn(List<Integer> values) {
            addCriterion("lib_id not in", values, "libId");
            return (Criteria) this;
        }

        public Criteria andLibIdBetween(Integer value1, Integer value2) {
            addCriterion("lib_id between", value1, value2, "libId");
            return (Criteria) this;
        }

        public Criteria andLibIdNotBetween(Integer value1, Integer value2) {
            addCriterion("lib_id not between", value1, value2, "libId");
            return (Criteria) this;
        }

        public Criteria andLibNameIsNull() {
            addCriterion("lib_name is null");
            return (Criteria) this;
        }

        public Criteria andLibNameIsNotNull() {
            addCriterion("lib_name is not null");
            return (Criteria) this;
        }

        public Criteria andLibNameEqualTo(String value) {
            addCriterion("lib_name =", value, "libName");
            return (Criteria) this;
        }

        public Criteria andLibNameNotEqualTo(String value) {
            addCriterion("lib_name <>", value, "libName");
            return (Criteria) this;
        }

        public Criteria andLibNameGreaterThan(String value) {
            addCriterion("lib_name >", value, "libName");
            return (Criteria) this;
        }

        public Criteria andLibNameGreaterThanOrEqualTo(String value) {
            addCriterion("lib_name >=", value, "libName");
            return (Criteria) this;
        }

        public Criteria andLibNameLessThan(String value) {
            addCriterion("lib_name <", value, "libName");
            return (Criteria) this;
        }

        public Criteria andLibNameLessThanOrEqualTo(String value) {
            addCriterion("lib_name <=", value, "libName");
            return (Criteria) this;
        }

        public Criteria andLibNameLike(String value) {
            addCriterion("lib_name like", value, "libName");
            return (Criteria) this;
        }

        public Criteria andLibNameNotLike(String value) {
            addCriterion("lib_name not like", value, "libName");
            return (Criteria) this;
        }

        public Criteria andLibNameIn(List<String> values) {
            addCriterion("lib_name in", values, "libName");
            return (Criteria) this;
        }

        public Criteria andLibNameNotIn(List<String> values) {
            addCriterion("lib_name not in", values, "libName");
            return (Criteria) this;
        }

        public Criteria andLibNameBetween(String value1, String value2) {
            addCriterion("lib_name between", value1, value2, "libName");
            return (Criteria) this;
        }

        public Criteria andLibNameNotBetween(String value1, String value2) {
            addCriterion("lib_name not between", value1, value2, "libName");
            return (Criteria) this;
        }

        public Criteria andLibLocationIsNull() {
            addCriterion("lib_location is null");
            return (Criteria) this;
        }

        public Criteria andLibLocationIsNotNull() {
            addCriterion("lib_location is not null");
            return (Criteria) this;
        }

        public Criteria andLibLocationEqualTo(String value) {
            addCriterion("lib_location =", value, "libLocation");
            return (Criteria) this;
        }

        public Criteria andLibLocationNotEqualTo(String value) {
            addCriterion("lib_location <>", value, "libLocation");
            return (Criteria) this;
        }

        public Criteria andLibLocationGreaterThan(String value) {
            addCriterion("lib_location >", value, "libLocation");
            return (Criteria) this;
        }

        public Criteria andLibLocationGreaterThanOrEqualTo(String value) {
            addCriterion("lib_location >=", value, "libLocation");
            return (Criteria) this;
        }

        public Criteria andLibLocationLessThan(String value) {
            addCriterion("lib_location <", value, "libLocation");
            return (Criteria) this;
        }

        public Criteria andLibLocationLessThanOrEqualTo(String value) {
            addCriterion("lib_location <=", value, "libLocation");
            return (Criteria) this;
        }

        public Criteria andLibLocationLike(String value) {
            addCriterion("lib_location like", value, "libLocation");
            return (Criteria) this;
        }

        public Criteria andLibLocationNotLike(String value) {
            addCriterion("lib_location not like", value, "libLocation");
            return (Criteria) this;
        }

        public Criteria andLibLocationIn(List<String> values) {
            addCriterion("lib_location in", values, "libLocation");
            return (Criteria) this;
        }

        public Criteria andLibLocationNotIn(List<String> values) {
            addCriterion("lib_location not in", values, "libLocation");
            return (Criteria) this;
        }

        public Criteria andLibLocationBetween(String value1, String value2) {
            addCriterion("lib_location between", value1, value2, "libLocation");
            return (Criteria) this;
        }

        public Criteria andLibLocationNotBetween(String value1, String value2) {
            addCriterion("lib_location not between", value1, value2, "libLocation");
            return (Criteria) this;
        }

        public Criteria andLibLongitudeIsNull() {
            addCriterion("lib_longitude is null");
            return (Criteria) this;
        }

        public Criteria andLibLongitudeIsNotNull() {
            addCriterion("lib_longitude is not null");
            return (Criteria) this;
        }

        public Criteria andLibLongitudeEqualTo(Double value) {
            addCriterion("lib_longitude =", value, "libLongitude");
            return (Criteria) this;
        }

        public Criteria andLibLongitudeNotEqualTo(Double value) {
            addCriterion("lib_longitude <>", value, "libLongitude");
            return (Criteria) this;
        }

        public Criteria andLibLongitudeGreaterThan(Double value) {
            addCriterion("lib_longitude >", value, "libLongitude");
            return (Criteria) this;
        }

        public Criteria andLibLongitudeGreaterThanOrEqualTo(Double value) {
            addCriterion("lib_longitude >=", value, "libLongitude");
            return (Criteria) this;
        }

        public Criteria andLibLongitudeLessThan(Double value) {
            addCriterion("lib_longitude <", value, "libLongitude");
            return (Criteria) this;
        }

        public Criteria andLibLongitudeLessThanOrEqualTo(Double value) {
            addCriterion("lib_longitude <=", value, "libLongitude");
            return (Criteria) this;
        }

        public Criteria andLibLongitudeIn(List<Double> values) {
            addCriterion("lib_longitude in", values, "libLongitude");
            return (Criteria) this;
        }

        public Criteria andLibLongitudeNotIn(List<Double> values) {
            addCriterion("lib_longitude not in", values, "libLongitude");
            return (Criteria) this;
        }

        public Criteria andLibLongitudeBetween(Double value1, Double value2) {
            addCriterion("lib_longitude between", value1, value2, "libLongitude");
            return (Criteria) this;
        }

        public Criteria andLibLongitudeNotBetween(Double value1, Double value2) {
            addCriterion("lib_longitude not between", value1, value2, "libLongitude");
            return (Criteria) this;
        }

        public Criteria andLibLatitudeIsNull() {
            addCriterion("lib_latitude is null");
            return (Criteria) this;
        }

        public Criteria andLibLatitudeIsNotNull() {
            addCriterion("lib_latitude is not null");
            return (Criteria) this;
        }

        public Criteria andLibLatitudeEqualTo(Double value) {
            addCriterion("lib_latitude =", value, "libLatitude");
            return (Criteria) this;
        }

        public Criteria andLibLatitudeNotEqualTo(Double value) {
            addCriterion("lib_latitude <>", value, "libLatitude");
            return (Criteria) this;
        }

        public Criteria andLibLatitudeGreaterThan(Double value) {
            addCriterion("lib_latitude >", value, "libLatitude");
            return (Criteria) this;
        }

        public Criteria andLibLatitudeGreaterThanOrEqualTo(Double value) {
            addCriterion("lib_latitude >=", value, "libLatitude");
            return (Criteria) this;
        }

        public Criteria andLibLatitudeLessThan(Double value) {
            addCriterion("lib_latitude <", value, "libLatitude");
            return (Criteria) this;
        }

        public Criteria andLibLatitudeLessThanOrEqualTo(Double value) {
            addCriterion("lib_latitude <=", value, "libLatitude");
            return (Criteria) this;
        }

        public Criteria andLibLatitudeIn(List<Double> values) {
            addCriterion("lib_latitude in", values, "libLatitude");
            return (Criteria) this;
        }

        public Criteria andLibLatitudeNotIn(List<Double> values) {
            addCriterion("lib_latitude not in", values, "libLatitude");
            return (Criteria) this;
        }

        public Criteria andLibLatitudeBetween(Double value1, Double value2) {
            addCriterion("lib_latitude between", value1, value2, "libLatitude");
            return (Criteria) this;
        }

        public Criteria andLibLatitudeNotBetween(Double value1, Double value2) {
            addCriterion("lib_latitude not between", value1, value2, "libLatitude");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}