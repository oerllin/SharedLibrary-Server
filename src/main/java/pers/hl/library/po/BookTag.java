package pers.hl.library.po;

import lombok.Data;

import java.util.List;

@Data
public class BookTag {
    private Integer id;

    private String name;

    private String remark;

    private List<Book> books;

}