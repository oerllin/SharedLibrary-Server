package pers.hl.library.po;

import lombok.Data;

import java.util.Date;

@Data
public class BookBorRec {

    /**
     * 借阅状态 1：借阅中 2：已借阅(接受) 3：已归还 4：已取消 5：已拒绝 999：无意义，用于标记只修改特殊信息(二维码id)
     */
    public static final int STATUS_BORROWING = 1;
    public static final int STATUS_BORROWED = 2;
    public static final int STATUS_RETURNED = 3;
    public static final int STATUS_CANCEL = 4;
    public static final int STATUS_REFUSED = 5;
    public static final int STATUS_NONE = 999;

    private Integer bbrId;

    private Integer bbrBorrower;

    private Date bbrTime;

    private Integer bbrBookId;

    private Integer bbrDuration;

    private Integer bbrStatus;

    private Book book;

    private Date returnTime;

    private Integer expiringDays;

}