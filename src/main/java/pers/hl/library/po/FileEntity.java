package pers.hl.library.po;

import lombok.Data;

import java.util.Date;

@Data
public class FileEntity {
    private Integer fileId;

    private String fileUrl;

    private Integer fileType;

    private Date fileCreateTime;

    private Date fileUpdateTime;

    private Integer fileBussId;

}