package pers.hl.library.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pers.hl.library.common.Const;
import pers.hl.library.common.Response;
import pers.hl.library.common.base.LibBaseController;
import pers.hl.library.po.Library;
import pers.hl.library.service.LibraryService;

import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping("lib")
public class LibraryController extends LibBaseController<Library> {

    @Autowired
    LibraryService libraryService;

    @Autowired
    public LibraryController(LibraryService libraryService) {
        init(libraryService);
    }

    @Override
    protected String getBussName() {
        return "图书馆";
    }

    /**
     * 查询附近图书馆
     *
     * @param distance 距离，单位：米。非必须参数
     */
    @GetMapping("nearby")
    public Response getNearbyLibs(@RequestParam(required = false, name = "distance") Double distance, @RequestParam("latitude") Double latitude, @RequestParam("longitude") Double longitude) {
        if (distance == null || distance < Const.DEFAULT_DISTANCE) {
            distance = (double) Const.DEFAULT_DISTANCE;
        }
        Map<String, Double> map = new LinkedHashMap<>();
        map.put("distance", distance);
        map.put("latitude", latitude);
        map.put("longitude", longitude);
        return Response.ok(libraryService.getNearbyLibs(map));
    }

    @Override
    protected void beforeAdd(Library data) throws Exception {
        data.setLibId(null);
    }
}
