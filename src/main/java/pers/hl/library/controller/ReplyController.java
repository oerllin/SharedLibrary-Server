package pers.hl.library.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pers.hl.library.common.base.LibBaseController;
import pers.hl.library.po.Reply;
import pers.hl.library.service.ReplyService;

import java.util.Date;

@RestController
@RequestMapping(("reply"))
public class ReplyController extends LibBaseController<Reply> {

    @Autowired
    ReplyService replyService;

    @Autowired
    public ReplyController(ReplyService replyService) {
        init(replyService);
    }

    @Override
    protected String getBussName() {
        return "回复";
    }

    @Override
    protected void beforeAdd(Reply data) throws Exception {
        data.setRplUserId(getCurrentUser().getUserId());
        data.setRplTime(new Date());
    }
}
