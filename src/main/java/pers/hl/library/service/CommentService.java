package pers.hl.library.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pers.hl.library.common.base.BaseService;
import pers.hl.library.dao.CommentMapper;
import pers.hl.library.dao.ReplyMapper;
import pers.hl.library.po.Comment;
import pers.hl.library.po.CommentExample;
import pers.hl.library.po.ReplyExample;

import java.util.List;

@Service
public class CommentService extends BaseService<Comment, CommentExample> {

    @Autowired
    private CommentMapper commentMapper;
    @Autowired
    private ReplyMapper replyMapper;

    @Autowired
    public CommentService(CommentMapper commentMapper) {
        init(commentMapper);
    }

    /**
     * 根据用户id或者书籍id查询评论列表
     */
    @Override
    public List<Comment> getDataList(String key, Integer otherId) {
        mExample = createExample();
        CommentExample.Criteria c = mExample.createCriteria();
        switch (key) {
            case "userId":
                c.andCmtUserIdEqualTo(otherId);
                break;
            case "bookId":
                c.andCmtBookIdEqualTo(otherId);
                break;
            default:
                return super.getDataList(key, otherId);
        }
        return dao.selectByExample(mExample);
    }

    @Override
    protected boolean afterDelete(Integer id) {
        ReplyExample example = new ReplyExample();
        example.createCriteria().andRplCmtIdEqualTo(id);
        return replyMapper.deleteByExample(example) > 0;
    }

    @Override
    protected boolean afterBatchDelete(List<Integer> ids) {
        for (Integer id : ids) {
            ReplyExample example = new ReplyExample();
            example.createCriteria().andRplCmtIdEqualTo(id);
            replyMapper.deleteByExample(example);
        }
        return true;
    }
}
