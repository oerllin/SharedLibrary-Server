package pers.hl.library.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import pers.hl.library.common.base.IMapper;
import pers.hl.library.po.FollowRec;
import pers.hl.library.po.FollowRecExample;
import pers.hl.library.po.User;

import java.util.List;

@Mapper
public interface FollowRecMapper extends IMapper<FollowRec, FollowRecExample> {
    int countByExample(FollowRecExample example);

    int deleteByExample(FollowRecExample example);

    int deleteByPrimaryKey(Integer frId);

    int insert(FollowRec record);

    int insertSelective(FollowRec record);

    List<FollowRec> selectByExample(FollowRecExample example);

    FollowRec selectByPrimaryKey(Integer frId);

    int updateByExampleSelective(@Param("record") FollowRec record, @Param("example") FollowRecExample example);

    int updateByExample(@Param("record") FollowRec record, @Param("example") FollowRecExample example);

    int updateByPrimaryKeySelective(FollowRec record);

    int updateByPrimaryKey(FollowRec record);

    List<User> userFollows(Integer userId);

    List<User> userFans(Integer userId);
}