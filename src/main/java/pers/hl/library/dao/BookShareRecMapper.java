package pers.hl.library.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import pers.hl.library.common.base.IMapper;
import pers.hl.library.po.BookShareRec;
import pers.hl.library.po.BookShareRecExample;

import java.util.List;

@Mapper
public interface BookShareRecMapper extends IMapper<BookShareRec, BookShareRecExample> {
    int countByExample(BookShareRecExample example);

    int deleteByExample(BookShareRecExample example);

    int deleteByPrimaryKey(Integer bsrId);

    int insert(BookShareRec record);

    int insertSelective(BookShareRec record);

    List<BookShareRec> selectByExample(BookShareRecExample example);

    BookShareRec selectByPrimaryKey(Integer bsrId);

    int updateByExampleSelective(@Param("record") BookShareRec record, @Param("example") BookShareRecExample example);

    int updateByExample(@Param("record") BookShareRec record, @Param("example") BookShareRecExample example);

    int updateByPrimaryKeySelective(BookShareRec record);

    int updateByPrimaryKey(BookShareRec record);

    List<BookShareRec> myPublish(Integer userId);

    List<BookShareRec> mySharing(Integer userId);
}