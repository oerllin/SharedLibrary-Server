package pers.hl.library.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import pers.hl.library.common.base.IMapper;
import pers.hl.library.po.BookTag;
import pers.hl.library.po.BookTagExample;

import java.util.List;

@Mapper
public interface BookTagMapper extends IMapper<BookTag, BookTagExample> {
    int countByExample(BookTagExample example);

    int deleteByExample(BookTagExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(BookTag record);

    int insertSelective(BookTag record);

    List<BookTag> selectByExample(BookTagExample example);

    BookTag selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") BookTag record, @Param("example") BookTagExample example);

    int updateByExample(@Param("record") BookTag record, @Param("example") BookTagExample example);

    int updateByPrimaryKeySelective(BookTag record);

    int updateByPrimaryKey(BookTag record);
}