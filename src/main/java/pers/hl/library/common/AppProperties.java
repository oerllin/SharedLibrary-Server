package pers.hl.library.common;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("app")
@Data
public class AppProperties {

    private AppProperties(){}

    private static class HOLDER {
        private static final AppProperties INSTANCE = new AppProperties();
    }

    public static AppProperties getInstance() {
        return HOLDER.INSTANCE;
    }

    private String jwtSecretKey;

}
