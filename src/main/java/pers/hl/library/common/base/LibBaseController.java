package pers.hl.library.common.base;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import pers.hl.library.common.Const;
import pers.hl.library.common.CustomException;
import pers.hl.library.common.Response;
import pers.hl.library.po.User;
import pers.hl.library.utils.LogUtils;
import pers.hl.library.utils.MyUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public abstract class LibBaseController<T> extends BaseController<T> {

    @Autowired
    public HttpServletRequest mRequest;

    @Override
    public void init(IService<T> service) {
        super.init(service);
    }

    /**
     * 获取当前用户
     */
    protected User getCurrentUser() {
        User curUser = (User) mRequest.getAttribute(Const.Keys.KEY_USER);
        if (curUser == null) {
            throw new CustomException(Const.HttpStatusCode.HttpStatus_500, "服务异常，无法获取当前用户信息，请尝试重新登录；若依然无效，请联系管理员。");
        }
        return curUser;
    }

    @GetMapping("/test")
    public Response test(String userId) {
        return Response.ok("测试成功，userId=" + userId);
    }

    @GetMapping("/getAllUrl")
    public List<String> getAllUrl() {
        User user = getCurrentUser();
        LogUtils.e("getAllUrl, user=" + user.toString());
        return MyUtils.getAllUrl();
    }

}
