package pers.hl.library.common.base;

import java.util.ArrayList;
import java.util.List;

public interface IService<T> {

    /**
     * 根据主键查询数据
     * @return 单个数据
     */
    T getDataByPrimaryId(Integer id);

    /**
     * 根据其他id查询数据
    * @return 单个数据，默认实现
     */
    default T getDataByOtherId(Integer otherId) {
        return null;
    }

    /**
     * 查询所有信息，默认实现
     * @return 数据集合
     * @param key 用于区分业务id
     * @param otherId 业务id的值
     */
    default List<T> getDataList(String key, Integer otherId){
        return new ArrayList<>();
    }

    /**
     * 根据id查询数据
     * @param id 根据业务逻辑传递id查询数据列表,默认实现
     * @return 是否成功
     */
    default List<T> getDataList(Integer id) {
        return new ArrayList<>();
    }

    /**
     * 可能有多种业务id，这里是第二个方法
     * 根据id查询数据
     *
     * @param way
     * @param key 根据业务逻辑传递指定字段查询数据列表,默认实现
     * @return 是否成功
     */
    default List<T> getDataList2(String key, String value) {
        return new ArrayList<>();
    }

    /**
     * 添加单个数据
     * 如果想返回主键id(直接返回到对应数据库字段的实体属性上的),tips:mybatis的selectKey标签配合sql语句就可以实现这一需求；
     * 或者是在insert标签中加入useGeneratedKeys和keyProperty属性也可以（useGeneratedKeys是使用生成的主键的意思）
     *
     * @param data 单个数据
     * @return 是否成功 mybatis insert成功返回1，失败返回0
     */
    boolean addData(T data);

    /**
     * 添加单个数据(选择性)，空字段不写入
     * 如果想返回主键id(直接返回到对应数据库字段的实体属性上的),tips:mybatis的selectKey标签配合sql语句就可以实现这一需求；
     * 或者是在insert标签中加入useGeneratedKeys和keyProperty属性也可以（useGeneratedKeys是使用生成的主键的意思）
     *
     * @param data 单个数据
     * @return 是否成功 mybatis insert成功返回1，失败返回0
     */
    boolean addDataSelective(T data);

    /**
     * 批量添加数据
     * @param list 数据集合
     * @return 是否成功
     */
    boolean batchAdd(List<T> list);

    /**
     * 根据id删除数据，可单个可以多个根据业务规则来
     * @param id 根据业务规则传递的id，不一定是主键
     * @return 是否成功
     */
    boolean deleteData(Integer id);

    /**
     * 根据id批量删除数据，
     * @param ids 主键id集合
     * @return 是否成功
     */
    boolean batchDelete(List<Integer> ids);

    /**
     * 根据主键更新数据信息
     * @param data 单个数据
     * @return 是否成功
     */
    boolean update(T data);

    /**
     * 根据主键更新数据信息(选择性，空字段不修改)
     * @param data 单个数据
     * @return 是否成功
     */
    boolean updateSelective(T data);

    /**
     * 根据条件更新数据信息
     * @param data 单个数据
     * @return 是否成功
     */
    boolean updateByExample(T data);

    /**
     * 根据条件更新数据信息(选择性，空字段不修改)
     * @param data 单个数据
     * @return 是否成功
     */
    boolean updateByExampleSelective(T data);

}
