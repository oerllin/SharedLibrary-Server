package pers.hl.library.common.base;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 *
 * @param <T> 对应实体类
 * @param <E> 对应实体Example类
 */
@Mapper
public interface IMapper<T, E extends IExample> {

    T getDataByPrimaryOtherId(Integer otherId);

    List<T> getDataListByOtherId(String id);

    int batchAdd(List<T> list);

    int batchDelete(List<Integer> ids);

    int countByExample(E example);

    int deleteByExample(E example);

    int deleteByPrimaryKey(Integer id);

    int insert(T record);

    int insertSelective(T record);

    List<T> selectByExample(E example);

    T selectByPrimaryKey(Integer id);

    int updateByExampleSelective(T record, E example);

    int updateByExample(T record, E example);

    int updateByPrimaryKeySelective(T record);

    int updateByPrimaryKey(T record);

}
