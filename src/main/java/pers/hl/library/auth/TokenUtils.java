package pers.hl.library.auth;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;

/**
 * token帮助类，使用JWT(JAVA WEB TOKEN)生成token
 *
 * JWT的组成：
 *  第一部分为头部（header)，第二部分我们称其为载荷（payload)，第三部分是签证（signature)。【中间用 . 分隔】
 * 验证流程：
 *  ① 在头部信息中声明加密算法和常量， 然后把header使用json转化为字符串
 *  ② 在载荷中声明用户信息，同时还有一些其他的内容；再次使用json 把载荷部分进行转化，转化为字符串
 *  ③ 使用在header中声明的加密算法和每个项目随机生成的secret来进行加密， 把第一步分字符串和第二部分的字符串进行加密， 生成新的字符串。此字符串是独一无二的。
 *  ④ 解密的时候，只要客户端带着JWT来发起请求，服务端就直接使用secret进行解密。
 *
 *  JWT详解：https://www.jianshu.com/p/e88d3f8151db
 */
public class TokenUtils {

    // header
    public static final String TOKEN_HEADER = "Authorization";
    // 过期时间是3600秒，即1个小时
    public static final long EXPIRATION = 3600L;
    // 选择了记住我之后的过期时间为7天
    public static final long EXPIRATION_REMEMBER = 604800L;

    private String secret;
    private String header;

    /**
     * 生成token
     *
     * @param subject
     * @return
     */
    public String createToken(String subject) {
        Date nowDate = new Date();
        Date expireDate = new Date(nowDate.getTime() + EXPIRATION * 1000);//过期时间

        return Jwts.builder() // 创建 JWT 对象
                .setHeaderParam("typ", "JWT")
                .setSubject(subject) // 设置主题（声明信息）
                .setIssuedAt(nowDate)
                .setExpiration(expireDate)
                .signWith(SignatureAlgorithm.HS512, secret) // 设置安全密钥（生成签名所需的密钥和算法）
                .compact(); // 生成token（1.编码 Header 和 Payload 2.生成签名 3.拼接字符串）
    }

    /**
     * 获取token中注册信息
     *
     * @param token
     * @return
     */
    public Claims getTokenClaim(String token) {
        try {
            return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 验证token是否过期失效
     *
     * @param expirationTime
     * @return
     */
    public boolean isTokenExpired(Date expirationTime) {
        return expirationTime.before(new Date());
    }

    /**
     * 获取token失效时间
     *
     * @param token
     * @return
     */
    public Date getExpirationDateFromToken(String token) {
        return getTokenClaim(token).getExpiration();
    }

    /**
     * 获取用户名从token中
     */
    public String getUsernameFromToken(String token) {
        return getTokenClaim(token).getSubject();
    }

    /**
     * 获取jwt发布时间
     */
    public Date getIssuedAtDateFromToken(String token) {
        return getTokenClaim(token).getIssuedAt();
    }

    // --------------------- getter & setter ---------------------

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

}
