package pers.hl.library.zxing;


import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageConfig;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import org.junit.jupiter.api.Test;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 二维码生成工具单元测试类
 */
public class QRCodeTest {

    /**
     * 二维码生成
     *
     * @throws WriterException
     * @throws IOException
     */
    @Test
    public void QREncode() throws WriterException, IOException {
        String content = "Hello ZXing!";
        int width = 200; // 宽度
        int height = 200; // 高度
        String format = "png"; // 图片类型
        Map<EncodeHintType, Object> hints = new HashMap<>();
        hints.put(EncodeHintType.CHARACTER_SET, "UTF-8"); // 设置内容编码格式
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H); // 设置纠错等级
        hints.put(EncodeHintType.MARGIN, 1); //设置二维码边的空度，非负数
        BitMatrix bitMatrix = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, width, height, hints);
        //MatrixToImageWriter.writeToPath(bitMatrix, format, new File("src/main/resources/static/image/qr/qr1.png").toPath());

        /*
        存在问题：能够正常生成二维码，但是生成二维码logo会变成黑白色
        原因：MatrixToImageConfig默认黑白，需要设置BLACK、WHITE
        解决：https://ququjioulai.iteye.com/blog/2254382
         */
        MatrixToImageConfig matrixToImageConfig = new MatrixToImageConfig(0xFF000001, 0xFFFFFFFF);
        BufferedImage bufferedImage = LogoMatrix(MatrixToImageWriter.toBufferedImage(bitMatrix, matrixToImageConfig), new File("src/main/resources/static/image/qrcode/ic_avatar_default.jpg"));
        ImageIO.write(bufferedImage, "png", new File("src/main/resources/static/image/qrcode/qr2.png"));
        System.out.println("输出成功");
    }

    /**
     * 二维码添加logo
     *
     * @param martixImage 源二维码图片
     * @param logoFile    logo图片
     * @return 返回带有logo的二维码图片
     * @throws IOException
     */
    @Test
    public BufferedImage LogoMatrix(BufferedImage martixImage, File logoFile) throws IOException {
        Graphics2D graphics2D = martixImage.createGraphics();
        int martixWidth = martixImage.getWidth();
        int martixHeight = martixImage.getHeight();


        // 读取Logo图片
        BufferedImage logo = ImageIO.read(logoFile);
        // 开始绘制图片
        graphics2D.drawImage(logo, martixWidth / 5 * 2, martixHeight / 5 * 2, martixWidth / 5, martixHeight / 5, null); // 绘制
        BasicStroke stroke = new BasicStroke(5, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
        graphics2D.setStroke(stroke); // 设置笔画对象
        // 指定弧度的圆角矩形
        RoundRectangle2D.Float round = new RoundRectangle2D.Float(martixWidth / 5 * 2, martixHeight / 5 * 2, martixWidth / 5, martixHeight / 5, 20, 20);
        graphics2D.setColor(Color.WHITE);
        graphics2D.draw(round); // 绘制圆弧矩形

        // 设置logo有一道灰色边框
        BasicStroke stroke2 = new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
        graphics2D.setStroke(stroke2); // 设置笔画对象
        RoundRectangle2D.Float round2 = new RoundRectangle2D.Float(martixWidth / 5 * 2 + 2, martixHeight / 5 * 2 + 2, martixWidth / 5 - 4, martixHeight / 5 - 4, 20, 20);
        graphics2D.setColor(new Color(128, 128, 128));
        graphics2D.draw(round2);


        graphics2D.dispose();
        martixImage.flush();
        return martixImage;
    }
}
