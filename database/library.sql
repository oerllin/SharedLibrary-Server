-- MySQL dump 10.13  Distrib 5.7.16, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: library
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book`
(
    `book_id`      int(11) NOT NULL AUTO_INCREMENT,
    `book_name`    varchar(255)     DEFAULT NULL COMMENT '书籍名称',
    `book_isbn`    bigint(20)       DEFAULT NULL COMMENT 'ISBN编码 书籍背面条形码',
    `book_owner`   int(11)          DEFAULT NULL COMMENT '书籍持有者(分享者)id',
    `book_file_id` int(11)          DEFAULT NULL COMMENT '书籍二维码文件id，唯一',
    `book_status`  int(11)          DEFAULT '0' COMMENT '书籍状态，0：可借阅 1：不可借阅',
    `book_desc`    longtext COMMENT '书籍描述,由分享人编辑',
    `book_author`  varchar(255)     DEFAULT NULL COMMENT '书籍作者',
    `book_tag`     int(11) NOT NULL DEFAULT '1' COMMENT '书籍标签id',
    `book_lib_id`  int(11) NOT NULL COMMENT '书籍所属图书馆id',
    `book_img`     varchar(255)     DEFAULT NULL COMMENT '书籍图片',
    PRIMARY KEY (`book_id`),
    UNIQUE KEY `book_book_file_id_uindex` (`book_file_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 7
  DEFAULT CHARSET = utf8 COMMENT ='书籍信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book`
    DISABLE KEYS */;
INSERT INTO `book`
VALUES (1, '编程实践', 9787111659624, 1, 1, 0,
        '本书的重点不仅在于讲解 Kotlin 的语法与语义，还将展示何时以及为何应该使用某个指定的语言特性。本书的目标不一定会涵盖每一个 Kotlin 语法与库的细节。但在最后，本书添加了很多基本原理的内容，让即使只有 Kotlin 初级知识的读者也可以理解本书的内容。当你认真学习 Kotlin 的细节后，你就可以使用这门受欢迎的语言构建 Android 应用、Web 应用以及其他程序。Kotlin 不仅可以用于任何应用 Java 的地方，还可以用于 iOS 开发、编写原生应用程序、JavaScript 开发以及更多场景。\n认真学习 Kotlin 的细节后，你就可以使用这门受欢迎的语言构建 Android 应用、Web 应用以及其他程序。通过学习本书，开发者们将学习到如何在自己的项目中使用这门基于Java的语言。无论你是经验丰富的编程人员还是学习 Kotlin 的新人，都将获益良多。\n作者 Ken Kousen（也是Modern Java Recipes一书的作者）将教授你如何专注于使用自己的 Kotlin 方案来解决问题而不是陷于基础语法之中。针对常见问题，本书将给出具体解决方案。Kotlin 在 Android 开发上已经大规模使用，但实际上 Kotlin 不仅可以用于任何应用 Java 的地方，还可以用于 iOS 开发、编写原生应用程序、JavaScript 开发以及更多场景。立即投入 Kotlin 的学习并开始创建新项目吧！\n通过本书，你将：\n●掌握函数式编程概念，包括 lambda 表达式、序列和并发。\n●学习如何使用委托、延迟初始化和作用域函数。\n●学习 Kotlin 与 Java 互操作的能力并使用 Kotlin 访问 Java 库。\n● 能够编写扩展函数。\n● 能够使用诸如 JUnit5 之类的实用库。\n● 在特定的开发框架（例如 Android 和 Spring）内获得实践经验。',
        'ken kousen', 2, 11, 'https://img9.doubanio.com/view/subject/m/public/s33694424.jpg'),
       (2, '雅舍小品', 9787201070797, 1, 2, 4,
        '《雅舍小品》主要内容简介：到四川来，觉得此地人建造房屋最是经济。火烧过的砖，常常用来做柱子，孤零零的砌起四根砖柱，上面盖上一个木头架子，看上去瘦骨嶙嶙，单薄得可怜，但是顶上铺了瓦，四面编了竹篦墙，墙上敷了泥灰，远远的看过去，没有人能说不像是座房子。我现在住的“雅舍”正是这样一座典型的房子。不消说，这房子有砖柱，有竹篦墙，一切特点都应有尽有。讲到住房，我的经验不算少，什么“上支下摘”，“前廊后厦”，“一楼一底”，“三上三下”，“亭子间”，“茅草棚”，“琼楼玉宇”和“摩天大厦”各式各样，我都尝试过。我不论住在哪里，只要住得稍久，对那房子便发生感情，非不得已我还舍不得搬，这“雅舍”，我初来时仅求其能蔽风雨，并不敢存奢望，现在住了两个多月，我的好感油然而生。虽然我已渐渐感觉它是并不能蔽风雨，因为有窗而无玻璃，风来则洞若凉亭，有瓦而空隙不少，雨来则渗如滴漏。纵然不能蔽风雨，“雅舍”还是自有它的个性。有个性就可爱。',
        '梁实秋', 2, 6, 'https://img1.doubanio.com/view/subject/m/public/s6806698.jpg'),
       (3, '非暴力沟通', 9787508086156, 1, 3, 3,
        '作为一个遵纪守法的好人，也许我们从来没有想过和“暴力”扯上关系。不过如果稍微留意一下现实生活中的谈话方式，并且用心体会各种谈话方式给我们的不同感受，我们一定会发现，有些话的确伤人！言语上的职责、嘲讽、否定、说教以及任意打断、拒不回应、随意出口的评价和结论给我们带来的情感和精神上的创伤，甚至比肉体的伤害更加令人痛苦。这些无心或有意的语言暴力让人与人变得冷漠、隔阂、敌视。',
        '马歇尔·卢森堡 (Marshall B.Rosenberg)', 5, 11, 'https://img9.doubanio.com/view/subject/m/public/s28423534.jpg'),
       (4, '见识', 9787508682235, 1, 4, 4,
        '《见识：商业的本质和人生的智慧》是根据吴军老师在“得到”App专栏的订阅用户最为关心的内容，将之重新补充、调整后的作品，作品增补、调整篇幅超过60%。\n吴军老师认为，与其他外部资源或者个人因素相比，个人的成就首先取决于“见识”。因此在书中，他将自己的经历，以及身边那些时代领航者的经验，以极其睿智的方式阐述出来，为你提供一个与众不同的、值得深度思考的看待世界、看待问题的视角。',
        '[美]吴军', 2, 11, 'https://img3.doubanio.com/view/subject/m/public/s29575321.jpg'),
       (5, '高效能人士的七个习惯（钻石版）', 9787515350622, 1, 5, 1, '这本书写的是高效能人士的七个习惯', '[美] 史蒂芬·柯维', 2, 11,
        'https://img9.doubanio.com/view/subject/m/public/s29968856.jpg'),
       (6, '金字塔原理', 9787544294829, 1, 6, 0,
        '《金字塔原理》介绍了一种能清晰地展现思路的高效方法，是训练思考、使表达呈现逻辑性的实用宝典。金字塔原理能将零散的观点有序组织起来，化繁为简，适合所有需要精进思考、分析、表达能力的读者。\n深入思考：建立金字塔思维，提取有价值的信息，找到问题的关键，将复杂的问题变得清晰简单。\n解决问题：从基本事实切入直击要点，制定严谨合理的解决方案，突破瓶颈。\n项目管理：明确目标，制定行动计划，根据MECE原则合理分配任务，不重叠、无遗漏。\n清晰表达：陈述项目、演讲、讨论时，清晰呈现自己的观点，说服听众，与上级、同事、客户迅速建立共识，高效沟通。\n轻松写作：挖掘读者的关注点、兴趣点、利益点，写出重点突出、条理鲜明的策划方案、分析报告、精彩文案和PPT，让人过目不忘。',
        '[美]芭芭拉•明托', 2, 6, 'https://img9.doubanio.com/view/subject/m/public/s32276605.jpg');
/*!40000 ALTER TABLE `book`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book_bor_rec`
--

DROP TABLE IF EXISTS `book_bor_rec`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_bor_rec`
(
    `bbr_id`       int(11)   NOT NULL AUTO_INCREMENT COMMENT '借阅记录id',
    `bbr_borrower` int(11)            DEFAULT NULL COMMENT '借阅人id',
    `bbr_time`     timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '借阅时间',
    `bbr_book_id`  int(11)            DEFAULT NULL COMMENT '借阅书籍id',
    `bbr_duration` int(11)            DEFAULT '30' COMMENT '借阅时长，单位【天】，默认30天',
    `bbr_status`   int(11)   NOT NULL COMMENT '1：借阅中 2：已借阅 3：已归还 4：已取消 5：已拒绝',
    `return_time`  date               DEFAULT NULL COMMENT '归还时间',
    PRIMARY KEY (`bbr_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 8
  DEFAULT CHARSET = utf8 COMMENT ='书籍借阅记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_bor_rec`
--

LOCK TABLES `book_bor_rec` WRITE;
/*!40000 ALTER TABLE `book_bor_rec`
    DISABLE KEYS */;
INSERT INTO `book_bor_rec`
VALUES (1, 7, '2021-03-07 14:05:55', 2, 30, 4, NULL),
       (2, 7, '2021-03-07 14:06:08', 2, 30, 4, NULL),
       (3, 7, '2021-03-07 14:06:19', 2, 30, 4, NULL),
       (4, 7, '2021-03-07 14:06:23', 4, 30, 4, NULL),
       (5, 7, '2021-03-07 14:55:54', 3, 30, 5, NULL),
       (6, 7, '2021-03-07 15:35:48', 3, 30, 3, NULL),
       (7, 7, '2021-03-07 15:01:55', 5, 30, 1, NULL);
/*!40000 ALTER TABLE `book_bor_rec`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book_share_rec`
--

DROP TABLE IF EXISTS `book_share_rec`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_share_rec`
(
    `bsr_id`          int(11)   NOT NULL AUTO_INCREMENT COMMENT '书籍分享记录id',
    `bsr_user_id`     int(11)            DEFAULT NULL COMMENT '分享人id',
    `bsr_remark`      varchar(255)       DEFAULT NULL COMMENT '备注：一般是联系方式',
    `bsr_create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '书籍分享时间，即线下扫码以后接受借阅的时间',
    `book_id`         int(11)   NOT NULL,
    PRIMARY KEY (`bsr_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 7
  DEFAULT CHARSET = utf8 COMMENT ='书籍分享记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_share_rec`
--

LOCK TABLES `book_share_rec` WRITE;
/*!40000 ALTER TABLE `book_share_rec`
    DISABLE KEYS */;
INSERT INTO `book_share_rec`
VALUES (1, 1, NULL, '2021-03-07 07:08:11', 1),
       (2, 1, NULL, '2021-03-07 07:08:48', 2),
       (3, 1, NULL, '2021-03-07 07:09:14', 3),
       (4, 1, NULL, '2021-03-07 07:10:11', 4),
       (5, 1, NULL, '2021-03-07 07:10:33', 5),
       (6, 1, NULL, '2021-03-07 07:10:47', 6);
/*!40000 ALTER TABLE `book_share_rec`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book_tag`
--

DROP TABLE IF EXISTS `book_tag`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_tag`
(
    `id`     int(11) NOT NULL AUTO_INCREMENT,
    `name`   varchar(255) DEFAULT NULL COMMENT '标签名',
    `remark` varchar(255) DEFAULT NULL COMMENT '备注\n',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 9
  DEFAULT CHARSET = utf8 COMMENT ='书籍标签';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_tag`
--

LOCK TABLES `book_tag` WRITE;
/*!40000 ALTER TABLE `book_tag`
    DISABLE KEYS */;
INSERT INTO `book_tag`
VALUES (1, '历史', '从世界各国的历史潮流汲取知识'),
       (2, '文学', '在文学的天地里发挥想象'),
       (3, '男生', '每个男孩都有一个梦'),
       (4, '女生', '每个女孩都有一个梦'),
       (5, '哲学', '与你探讨世间万物的意义'),
       (6, '武侠', '领略大家笔下熠熠生辉的江湖意气');
/*!40000 ALTER TABLE `book_tag`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `collect`
--

DROP TABLE IF EXISTS `collect`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `collect`
(
    `collect_id`  int(11)   NOT NULL AUTO_INCREMENT COMMENT '主键',
    `book_id`     int(11)   NOT NULL COMMENT '书籍id',
    `user_id`     int(11)   NOT NULL COMMENT '用户id',
    `create_time` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`collect_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8 COMMENT ='点赞（收藏）记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `collect`
--

LOCK TABLES `collect` WRITE;
/*!40000 ALTER TABLE `collect`
    DISABLE KEYS */;
INSERT INTO `collect`
VALUES (3, 4, 1, '2021-03-07 14:18:43');
/*!40000 ALTER TABLE `collect`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment`
(
    `cmt_id`      int(11)   NOT NULL AUTO_INCREMENT COMMENT '评论记录id',
    `cmt_user_id` int(11)            DEFAULT NULL COMMENT '评论人id',
    `cmt_info`    varchar(255)       DEFAULT NULL COMMENT '评论内容',
    `cmt_book_id` int(11)            DEFAULT NULL COMMENT '评论关联书籍id',
    `cmt_time`    timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '评论时间',
    PRIMARY KEY (`cmt_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 2
  DEFAULT CHARSET = utf8 COMMENT ='评论信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment`
    DISABLE KEYS */;
INSERT INTO `comment`
VALUES (1, 7, '这是我对剑士的第一条评论', 4, '2021-03-07 14:07:07');
/*!40000 ALTER TABLE `comment`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file`
--

DROP TABLE IF EXISTS `file`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file`
(
    `file_id`          int(11)   NOT NULL AUTO_INCREMENT COMMENT '文件id',
    `file_url`         varchar(255)       DEFAULT NULL COMMENT '文件地址，具体到服务器文件路径',
    `file_type`        int(11)            DEFAULT NULL COMMENT '文件类型 1：书籍照片 2：书籍二维码 3：用户头像',
    `file_create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `file_update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    `file_buss_id`     int(11)   NOT NULL COMMENT '关联的业务id',
    PRIMARY KEY (`file_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 7
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file`
--

LOCK TABLES `file` WRITE;
/*!40000 ALTER TABLE `file`
    DISABLE KEYS */;
INSERT INTO `file`
VALUES (1, 'D:\\share_library\\file\\qrcode\\eb6f7fc28f844481b9f05b47b9603cbf1615100890356.png', 2,
        '2021-03-07 07:08:10', '2021-03-07 07:08:10', 1),
       (2, 'D:\\share_library\\file\\qrcode\\5c8f1eca56be46fb8a45a15c812759c41615100927778.png', 2,
        '2021-03-07 07:08:48', '2021-03-07 07:08:48', 2),
       (3, 'D:\\share_library\\file\\qrcode\\ba475df0df8e46b89c7887e54541f5c01615100954121.png', 2,
        '2021-03-07 07:09:14', '2021-03-07 07:09:14', 3),
       (4, 'D:\\share_library\\file\\qrcode\\819de1c86cea42cfb0e272e6c75d8e081615101010807.png', 2,
        '2021-03-07 07:10:11', '2021-03-07 07:10:11', 4),
       (5, 'D:\\share_library\\file\\qrcode\\22e308da30f3473eafdb6a261651f0281615101033425.png', 2,
        '2021-03-07 07:10:33', '2021-03-07 07:10:33', 5),
       (6, 'D:\\share_library\\file\\qrcode\\e5f56a26f05f481c873f46c8bf6d8ff91615101047195.png', 2,
        '2021-03-07 07:10:47', '2021-03-07 07:10:47', 6);
/*!40000 ALTER TABLE `file`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `follow_rec`
--

DROP TABLE IF EXISTS `follow_rec`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `follow_rec`
(
    `fr_id`          int(11)   NOT NULL AUTO_INCREMENT COMMENT '关注记录id',
    `fr_user_id`     int(11)            DEFAULT NULL COMMENT '用户ID',
    `fr_follower_id` int(11)            DEFAULT NULL COMMENT '关注用户ID',
    `fr_create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `fr_update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    PRIMARY KEY (`fr_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='用户关注记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `follow_rec`
--

LOCK TABLES `follow_rec` WRITE;
/*!40000 ALTER TABLE `follow_rec`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `follow_rec`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `library`
--

DROP TABLE IF EXISTS `library`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `library`
(
    `lib_id`        int(11) NOT NULL AUTO_INCREMENT COMMENT '共享图书馆id',
    `lib_name`      varchar(255) DEFAULT NULL COMMENT '共享图书馆名(社区名)',
    `lib_location`  varchar(255) DEFAULT NULL COMMENT '位置',
    `lib_longitude` double       DEFAULT NULL COMMENT '经度',
    `lib_latitude`  double       DEFAULT NULL COMMENT '纬度',
    PRIMARY KEY (`lib_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 13
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `library`
--

LOCK TABLES `library` WRITE;
/*!40000 ALTER TABLE `library`
    DISABLE KEYS */;
INSERT INTO `library`
VALUES (5, '软件园c3图书馆', '四川省成都市武侯区桂溪街道西门子(中国)有限公司成都分公司天府软件园C区', 104.071571, 30.538837),
       (6, '软件园A区图书馆', '四川省成都市武侯区桂溪街道世纪城南路英郡', 104.073638, 30.548686),
       (7, '绿地之窗图书馆', '四川省成都市武侯区桂溪街道中信银行(天府支行)四川富润国际广场', 104.06147, 30.551919),
       (8, 'OCG图书馆', '四川省成都市武侯区桂溪街道中国铁塔股份有限公司四川省分公司携程信息技术大楼(天府四街)', 104.064743, 30.541841),
       (11, '华府大道图书馆', '四川省成都市双流区华阳镇街道天府大道南段锐力·领峰', 104.069854, 30.525033);
/*!40000 ALTER TABLE `library`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reply`
--

DROP TABLE IF EXISTS `reply`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reply`
(
    `rpl_id`      int(11)   NOT NULL AUTO_INCREMENT COMMENT '回复id',
    `rpl_cmt_id`  int(11)        DEFAULT NULL COMMENT '评论记录id',
    `rpl_user_id` int(11)        DEFAULT NULL COMMENT '回复人id',
    `rpl_info`    varchar(255)   DEFAULT NULL COMMENT '回复记录',
    `rpl_time`    timestamp NULL DEFAULT NULL COMMENT '回复时间',
    PRIMARY KEY (`rpl_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8 COMMENT ='回复表（针对评论）';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reply`
--

LOCK TABLES `reply` WRITE;
/*!40000 ALTER TABLE `reply`
    DISABLE KEYS */;
INSERT INTO `reply`
VALUES (1, 1, 7, '这是第一条回复', '2021-03-07 14:08:09'),
       (2, 1, 1, '本尊回复', '2021-03-07 14:18:26'),
       (3, 1, 1, '哦，是吗？\n', '2021-03-07 15:09:04');
/*!40000 ALTER TABLE `reply`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user`
(
    `user_id`     int(11) NOT NULL AUTO_INCREMENT,
    `user_name`   varchar(255) DEFAULT NULL,
    `user_gender` int(11)      DEFAULT NULL COMMENT '性别，1：男 2：女',
    `use_age`     int(11)      DEFAULT NULL COMMENT '年龄',
    `user_addr`   varchar(255) DEFAULT NULL COMMENT '用户地址',
    `user_credit` int(11)      DEFAULT NULL COMMENT '用户信用',
    `user_role`   int(11)      DEFAULT '1' COMMENT '用户角色 1：普通 2：管理员',
    `user_tel`    varchar(11)  DEFAULT NULL COMMENT '电话',
    `account`     varchar(255) DEFAULT NULL COMMENT '用户账号',
    `password`    varchar(255) DEFAULT NULL COMMENT '用户密码',
    PRIMARY KEY (`user_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 9
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'测试',1,18,'四川成都',100,1,'11111111111','test','e10adc3949ba59abbe56e057f20f883e'),(2,'管理员',1,50,'北京西城',100,2,'13300990099','admin','21232f297a57a5a743894a0e4a801fc3'),(7,NULL,NULL,NULL,NULL,NULL,1,NULL,'test1','e10adc3949ba59abbe56e057f20f883e'),(8,NULL,NULL,NULL,NULL,NULL,1,NULL,'test2','e10adc3949ba59abbe56e057f20f883e');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-08  0:02:38
